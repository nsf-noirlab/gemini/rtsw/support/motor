%define _prefix /gem_base/epics/support
%define name motor
%define repository gemdev
%define debug_package %{nil}
%define arch %(uname -m)
%define checkout %(git log --pretty=format:'%h' -n 1) 

# These defines need to be adjusted to point to the git ref
# that is to be built

# vendor/upstream git project
%define vendor_project https://github.com/epics-modules/motor.git
# vendor git ref (tag or commit hash). Please keep in sync with 'Version' below!
%define vendor_ref 6ee5f4eb

#These global defines are added to prevent stripping
# symbols on vxWorks cross-compiled code
# Getting 'strip' to work is probably only needed for
# building a related debug sub-package
#
# But this prevents all the strip warnings
# mrippa 20120202
%global _enable_debug_package 0
%global debug_package %{nil}
%global __os_install_post /usr/lib/rpm/brp-compress %{nil}

Summary: %{name} Package, a module for EPICS base
Name: %{name}
Version: 7.2.2.6ee5f4eb
Release: 0%{?dist}
License: EPICS Open License
Group: Applications/Engineering
Source0: %{name}-%{version}.tar.gz
ExclusiveArch: %{arch}
Prefix: %{_prefix}
## You may specify dependencies here
BuildRequires: epics-base-devel re2c gemini-ade asyn-devel busy-devel sequencer-devel
Requires: epics-base asyn busy sequencer
## Switch dependency checking off
# AutoReqProv: no

%description
This is the module %{name}.

## If you want to have a devel-package to be generated uncomment the following:
%package devel
Summary: %{name}-devel Package
Group: Development/Gemini
Requires: %{name}
%description devel
This is the module %{name}.

%prep
%setup -q 

%build
# get vendor code
git clone %{vendor_project} vendor_project
cd vendor_project
git submodule init
git submodule update modules/motorNewFocus
git submodule update modules/motorNewport
git submodule update modules/motorOms

# apply Gemini-specific configuration
cp ../configure/* configure/
cp -rf ../modules/* modules/

# fix freezing build
rm modules/CONFIG_SITE.local

make

%install
# cd into the directory containing the vendor sources
cd vendor_project

export DONT_STRIP=1
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r db $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r dbd $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r lib $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r include $RPM_BUILD_ROOT/%{_prefix}/%{name}
cp -r configure $RPM_BUILD_ROOT/%{_prefix}/%{name}
echo $RPM_BUILD_ROOT/%{_prefix}/%{name}
# newport
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorNewport
cp -r modules/motorNewport/bin $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorNewport
cp -r modules/motorNewport/db $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorNewport
cp -r modules/motorNewport/dbd $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorNewport
cp -r modules/motorNewport/lib $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorNewport
cp -r modules/motorNewport/include $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorNewport
cp -r modules/motorNewport/configure $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorNewport
# newfocus
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorNewFocus
cp -r modules/motorNewFocus/dbd $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorNewFocus
cp -r modules/motorNewFocus/lib $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorNewFocus
# oms
mkdir -p $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorOms
cp -r modules/motorOms/dbd $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorOms
cp -r modules/motorOms/lib $RPM_BUILD_ROOT/%{_prefix}/%{name}/modules/motorOms

%postun
if [ "$1" = "0" ]; then
	rm -rf %{_prefix}/%{name}
fi

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
   /%{_prefix}/%{name}/lib
   /%{_prefix}/%{name}/db
   /%{_prefix}/%{name}/modules/motorNewport/bin
   /%{_prefix}/%{name}/modules/motorNewport/db
   /%{_prefix}/%{name}/modules/motorNewport/lib
   /%{_prefix}/%{name}/modules/motorNewFocus/lib
   /%{_prefix}/%{name}/modules/motorOms/lib

%files devel
%defattr(-,root,root)
   /%{_prefix}/%{name}/dbd
   /%{_prefix}/%{name}/include
   /%{_prefix}/%{name}/configure
   /%{_prefix}/%{name}/modules/motorNewport/dbd
   /%{_prefix}/%{name}/modules/motorNewport/include
   /%{_prefix}/%{name}/modules/motorNewport/configure
   /%{_prefix}/%{name}/modules/motorNewFocus/dbd
   /%{_prefix}/%{name}/modules/motorOms/dbd

%changelog
